#include <encoderMatrix.h>

#define threshold 1500

long lastUpdate = 0;

const uint8_t xPinout[] = { 4, 5, 6, 7 };
const uint8_t yPinout[] = { 0, 1, 2, 3 };

EncoderMatrix encoderMatrix(xPinout, yPinout);

void setup() {
  Serial.begin(9600);

  Serial.println("------------------------");
  Serial.println("Encoder matrix example :");
  Serial.println("------------------------");
  Serial.println(" ");
  Serial.print("How many? ");
  Serial.println(encoderMatrix.howMany);
  Serial.println(" ");
}

void loop()
{
  if (micros() - lastUpdate > threshold)
  {
    encoderMatrix.update();

    for (int i = 0; i < encoderMatrix.howMany; i++) {
      // Check if val has changed
      if (encoderMatrix.hasChanged(i)) {
        // Send USB-MIDI-CC message
        usbMIDI.sendControlChange(i, encoderMatrix.getValue(i), 0, 0);

        /*
           DEBUG INFOS
        */
        Serial.print("Encoder #");
        Serial.print(i + 1, DEC);
        Serial.print("\t");
        Serial.print(encoderMatrix.getValue(i));
        Serial.print("\t");
        Serial.print("t=");
        Serial.print(micros());
        Serial.print("\t");
        Serial.print("∆=");
        Serial.println(micros() - lastUpdate);
      }
    }

    lastUpdate = micros();
  }
}
