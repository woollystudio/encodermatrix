#ifndef ENCODER_MATRIX_H__
#define ENCODER_MATRIX_H__

#include "WProgram.h"

const uint8_t QUADRATURE[] = { 0, 1, 3, 2 };

struct Encoder {
  int8_t count;
  int val;
  byte phase;
  bool hasChanged;
  byte pin[3];
};

enum {
  A,
  B,
  C
};

enum {
  ABS,
  ORD
};

class EncoderMatrix
{
  public:
    EncoderMatrix(const uint8_t *xPins, const uint8_t *yPins);
    void update();
    bool hasChanged(uint8_t index);
    int getValue(uint8_t index);
    uint8_t howMany;
  private:
    uint8_t _orderX;
    uint8_t _orderY;
    struct Encoder _encoder[8];
};


#endif /* ENCODER_MATRIX_H__ */
