#include "EncoderMatrix.h"

EncoderMatrix::EncoderMatrix(const uint8_t *xPins, const uint8_t *yPins)
{
  _orderX = sizeof(xPins) / sizeof(uint8_t);
  _orderY = sizeof(yPins) / sizeof(uint8_t);

  howMany = _orderX * _orderY / 2;

  for (uint8_t i = 0; i < howMany; i++)
  {
    _encoder[i].val = 0;
    _encoder[i].phase = 0;
    _encoder[i].count = 0;
    _encoder[i].hasChanged = false;

    _encoder[i].pin[A] = xPins[(i * 2) % _orderX];
    _encoder[i].pin[B] = xPins[(i * 2 + 1) % _orderX];
    _encoder[i].pin[C] = yPins[(i * 2) / _orderY];
  }

  for (uint8_t i = 0; i < _orderX; i++)
    pinMode(xPins[i], INPUT_PULLUP);

  for (uint8_t i = 0; i < _orderY; i++) {
    pinMode(yPins[i], OUTPUT);
    digitalWrite(yPins[i], HIGH);
  }
}

void EncoderMatrix::update() {
  // For each encoders
  for (uint8_t i = 0; i < howMany; i++) {
    // Set the common pin to LOW
    digitalWrite(_encoder[i].pin[C], LOW);

    uint8_t state = 0;

    // Read the A and B pin state
    const uint8_t A_STATE = !digitalRead(_encoder[i].pin[A]);
    const uint8_t B_STATE = !digitalRead(_encoder[i].pin[B]);

    // Write both states
    // in a single variable
    bitWrite(state, 0, A_STATE);
    bitWrite(state, 1, B_STATE);

    // Get the current state
    // with the quadrature sequence
    const byte thisPhase = QUADRATURE[state];

    // Check if current phase
    // is the same as the old one
    if (_encoder[i].phase != thisPhase) {
      // +1 if phase 3 >>> 0
      if (thisPhase == 0 && _encoder[i].phase == 3)
        _encoder[i].count += 1;
      // -1 if phase 0 >>> 3
      else if (thisPhase == 3 && _encoder[i].phase == 0)
        _encoder[i].count -= 1;
      // +1 if phase++
      else if (thisPhase > _encoder[i].phase)
        _encoder[i].count += 1;
      // -1 if phase--
      else if (thisPhase < _encoder[i].phase)
        _encoder[i].count -= 1;

      // If counts the entire sequence
      if (abs(_encoder[i].count) > 3) {
        _encoder[i].val += (_encoder[i].count / abs(_encoder[i].count));
        _encoder[i].hasChanged = true;
        _encoder[i].count = 0;
      }

      // If not
      else _encoder[i].hasChanged = false;

      // Update with current phase
      _encoder[i].phase = thisPhase;
    }

    // If not
    else _encoder[i].hasChanged = false;

    // Set common pin to HIGH
    digitalWrite(_encoder[i].pin[C], HIGH);
  }
}

bool EncoderMatrix::hasChanged(uint8_t index)
{
  return _encoder[index].hasChanged;
}

int EncoderMatrix::getValue(uint8_t index)
{
  return _encoder[index].val;
}
